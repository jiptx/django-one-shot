from django.urls import path
from todos.views import (
    show_TodoList,
    detail_TodoList,
    create_TodoList,
    update_TodoList,
    delete_TodoList,
    create_TodoItem,
    update_TodoItem,
)

urlpatterns = [
    path("", show_TodoList, name="show_Todolist"),
    path("<int:id>/", detail_TodoList, name="detail_TodoList"),
    path("create/", create_TodoList, name="create_TodoList"),
    path("<int:id>/edit/", update_TodoList, name="update_TodoList"),
    path("<int:id>/delete/", delete_TodoList, name="delete_TodoList"),
    path("items/create/", create_TodoItem, name="create_TodoItem"),
    path("items/<int:id>/edit/", update_TodoItem, name="update_TodoItem"),
]
