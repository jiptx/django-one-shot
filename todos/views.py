from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.


def show_TodoList(request):
    Todolist_list = TodoList.objects.all()
    context = {"Todolist_list": Todolist_list}
    return render(request, "todos/list.html", context)


def detail_TodoList(request, id):
    TodoList_lists = TodoList.objects.get(id=id)
    context = {
        "TodoList_lists": TodoList_lists,
    }
    return render(request, "todos/detail.html", context)


def create_TodoList(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList_lists = form.save()
            return redirect("detail_TodoList", id=TodoList_lists.id)

    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)


def update_TodoList(request, id):
    TodoList_lists = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=TodoList_lists)
        if form.is_valid():
            TodoList_lists = form.save()
            return redirect("detail_TodoList", id=TodoList_lists.id)

    else:
        form = TodoListForm(instance=TodoList_lists)
        context = {
            "form": form,
        }
    return render(request, "todos/update.html", context)


def delete_TodoList(request, id):
    if request.method == "POST":
        TodoList_lists = TodoList.objects.get(id=id)
        TodoList_lists.delete()
        return redirect(show_TodoList)
    return render(request, "todos/delete.html")


def create_TodoItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("detail_TodoList", id=todoitem.list.id)

    else:
        form = TodoItemForm()
        context = {
            "form": form,
        }
    return render(request, "todos/items/create.html", context)


def update_TodoItem(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("detail_TodoList", id=item.list.id)

    else:
        form = TodoItemForm(instance=item)
        context = {
            "form": form,
        }
    return render(request, "todos/items/update.html", context)
